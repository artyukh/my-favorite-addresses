﻿using Android.App;
using Android.OS;
using MvvmCross.Platforms.Android.Views;
using MyFavoriteAddresses.Core.ViewModels;

namespace MyFavoriteAddresses
{
    [Activity(Label = "New address")]
    public class NewAddressActivity : MvxActivity<NewAddressViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            SetContentView(Resource.Layout.activity_new_address);
        }       
    }
}
