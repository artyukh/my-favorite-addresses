﻿using System.Collections.Generic;
using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MyFavoriteAddresses.Core.Models;
using MyFavoriteAddresses.Core.ViewModels;

namespace MyFavoriteAddresses
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : MvxActivity<MainViewModel>, IOnMapReadyCallback
    {
        private GoogleMap _map;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            FragmentManager.FindFragmentById<MapFragment>(Resource.Id.googlemap).GetMapAsync(this);    
        }

        private void AddressesLoaded(List<LocationModel> locations)
        {
            _map.Clear();
            foreach (var item in locations)
            {
                var latlng = new LatLng(item.Latitude, item.Longitude);
                var options = new MarkerOptions().SetPosition(latlng).SetTitle(item.Address);
                _map.AddMarker(options);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            _map = googleMap;
            ViewModel.AddressesLoaded += AddressesLoaded;
            ViewModel.AddressesSelected += AddressesSelected;
            ViewModel.LoadAddresses();
        }

        private void AddressesSelected(LocationModel location)
        {
            var latlng = new LatLng(location.Latitude, location.Longitude);
            var camera = CameraUpdateFactory.NewLatLngZoom(latlng, 15);
            _map.AnimateCamera(camera);
        }
    }
}

