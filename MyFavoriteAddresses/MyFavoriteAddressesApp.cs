﻿using System;
using Android.App;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Views;
using MyFavoriteAddresses.Core;

namespace MyFavoriteAddresses
{
    [Application]
    public class MyFavoriteAddressesApp : MvxAndroidApplication<MvxAndroidSetup<App>, App>
    {
        public MyFavoriteAddressesApp(IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            
        }
    }
}
