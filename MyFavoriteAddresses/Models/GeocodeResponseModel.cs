﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MyFavoriteAddresses.Models
{
    public class GeocodeResponseModel : ResponseModel
    {
        [JsonProperty("results")]
        public List<GeocodeResultModel> Results { get; set; }
    }
}
