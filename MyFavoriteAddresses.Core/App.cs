﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using MyFavoriteAddresses.Core.Services.Services.WebApiService;
using MyFavoriteAddresses.Core.Services.WebApiService;

namespace MyFavoriteAddresses.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Client")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.IoCProvider.RegisterSingleton<IWebApiService>(Mvx.IoCProvider.IoCConstruct<WebApiService>());

            RegisterCustomAppStart<AppStart>();
        }
    }
}
