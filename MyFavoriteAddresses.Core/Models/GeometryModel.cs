﻿using System;
using Newtonsoft.Json;

namespace MyFavoriteAddresses.Core.Models
{
    public class GeometryModel
    {
        [JsonProperty("location")]
        public LocationModel Location { get; set; }
    }
}
