﻿using Newtonsoft.Json;
using Realms;

namespace MyFavoriteAddresses.Core.Models
{
    public class LocationModel : RealmObject
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lng")]
        public double Longitude { get; set; }

        public string Address { get; set; }
    }
}
