﻿using Newtonsoft.Json;

namespace MyFavoriteAddresses.Core.Models
{
    public class ResponseModel
    {
        [JsonProperty("status")]
        public string Status { get; set; } = string.Empty;

        public bool Success => Status.Equals("OK");

        public string ErrorMessage { get; set; }
    }
}
