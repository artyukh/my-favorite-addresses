﻿using Newtonsoft.Json;

namespace MyFavoriteAddresses.Core.Models
{
    public class GeocodeResultModel
    {
        [JsonProperty("formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonProperty("geometry")]
        public GeometryModel Geometry { get; set; }
    }
}
