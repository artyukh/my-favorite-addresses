﻿using MyFavoriteAddresses.Core.Models;
using MyFavoriteAddresses.Core.Services.Services.WebApiService;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using Refit;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyFavoriteAddresses.Core.Services.WebApiService
{
    public class WebApiService : IWebApiService
    {
        private const string ARI_URL = "https://maps.googleapis.com/maps";
        private const string API_KEY = "AIzaSyDvB3sDsiDd7_dL4mvAfD7HUr65r3hfiYQ";

        public bool IsBusy { get; set; }

        private readonly IWebApi _api;
        private readonly HttpClient _client;
        private AsyncRetryPolicy RetryPolicy;
        private AsyncCircuitBreakerPolicy BreakPolicy;
        private Task<T> BreakOrRetry<T>(Func<Task<T>> func) => Policy.WrapAsync(RetryPolicy, BreakPolicy).ExecuteAsync(func);

        public WebApiService()
        {
            try
            {
                _client = new HttpClient(new LoggingMessageHandler(null))
                {
                    BaseAddress = new Uri(ARI_URL)
                };

                _api = RestService.For<IWebApi>(_client);
                EncodingProvider provider = new CustomUtf8EncodingProvider();
                Encoding.RegisterProvider(provider);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception: " + ex.Message);
            }

            SetPolicy();
        }

        private void SetPolicy()
        {
            BreakPolicy = Policy.Handle<OperationCanceledException>()
            .Or<ApiException>()
            .CircuitBreakerAsync(1, TimeSpan.FromSeconds(1),
                (ex, timeSpan, context) =>
                {
                    var exType = ex.GetType();
                    Debug.WriteLine($"Circuit break: {exType} {ex.Message}");
                },
                context =>
                {
                    Debug.WriteLine($"Reset circuit");
                });

            RetryPolicy = Policy.Handle<ApiException>()
                .Or<WebException>()
                .WaitAndRetryAsync(2, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                (ex, timeSpan, context) =>
                {
                    var exType = ex.GetType();
                    Debug.WriteLine($"Something went wrong: {exType} {ex.Message}, retrying...");
                });
        }

        public async Task<T> Do<T>(Func<Task<T>> apiFunction, string loadingMessage = null) where T : ResponseModel, new()
        {
            var result = new T();
            try
            {
                IsBusy = true;
                result = await apiFunction();
            }
            catch (OperationCanceledException e)
            {
                (result as ResponseModel).ErrorMessage = e.Message;
            }
            catch (ApiException apiException)
            {
                (result as ResponseModel).ErrorMessage = apiException.Message;
            }
            catch (BrokenCircuitException brokenCircuitException)
            {
                (result as ResponseModel).ErrorMessage = brokenCircuitException.Message;
            }
            catch (Exception e)
            {
                (result as ResponseModel).ErrorMessage = "Something went wrong! - " + e.Message;
            }
            finally
            {
                IsBusy = false;
            }

            return result;
        }

        public async Task<GeocodeResponseModel> Geocode(string Address)
        {
            return await BreakOrRetry(async () => await _api.Geocode(Address, API_KEY));
        }     
    }

    public class CustomUtf8EncodingProvider : EncodingProvider
    {
        public override Encoding GetEncoding(string name)
        {
            return name == "utf8" ? Encoding.UTF8 : null;
        }

        public override Encoding GetEncoding(int codepage)
        {
            return null;
        }
    }
}
