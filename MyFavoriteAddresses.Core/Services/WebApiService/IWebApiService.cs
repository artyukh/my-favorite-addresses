﻿using System;
using System.Threading.Tasks;
using MyFavoriteAddresses.Core.Models;

namespace MyFavoriteAddresses.Core.Services.Services.WebApiService
{
    public interface IWebApiService
    {
        Task<T> Do<T>(Func<Task<T>> apiFunction, string loadingMessage = null) where T : ResponseModel, new();

        bool IsBusy { get; set; }

        Task<GeocodeResponseModel> Geocode(string Address);
    }
}