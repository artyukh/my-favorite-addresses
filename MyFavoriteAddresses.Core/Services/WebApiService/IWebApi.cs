﻿using System.Threading.Tasks;
using MyFavoriteAddresses.Core.Models;
using Refit;

namespace MyFavoriteAddresses.Core.Services.WebApiService
{
    public interface IWebApi
    {
        [Get("/api/geocode/json?")]
        Task<GeocodeResponseModel> Geocode(string address, string key);       
    }
}