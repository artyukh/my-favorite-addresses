﻿using MvvmCross.ViewModels;
using MyFavoriteAddresses.Core.Models;

namespace MyFavoriteAddresses.Core.ViewModels
{
    public class AddressViewModel : MvxNotifyPropertyChanged
    {
        private string _address;
        public string Address
        {
            get => _address;
            set => SetProperty(ref _address, value);
        }

        public LocationModel Location { get; set; }
    }
}
