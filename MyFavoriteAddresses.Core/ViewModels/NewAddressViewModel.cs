﻿using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyFavoriteAddresses.Core.Models;
using MyFavoriteAddresses.Core.Services.Services.WebApiService;
using Realms;

namespace MyFavoriteAddresses.Core.ViewModels
{
    public class NewAddressViewModel : MvxViewModel
    {
        private readonly IWebApiService _webApiService;
        private readonly IMvxNavigationService _navigationService;

        public NewAddressViewModel(IWebApiService webApiService, IMvxNavigationService navigationService)
        {
            _webApiService = webApiService;
            _navigationService = navigationService;
        }

        public override Task Initialize()
        {
            Addresses = new MvxObservableCollection<AddressViewModel>();
            return base.Initialize();
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                RaisePropertyChanged(() => SearchText);
                SearchAddress(value);
            }
        }

        private MvxObservableCollection<AddressViewModel> _addresses;
        public MvxObservableCollection<AddressViewModel> Addresses
        {
            get => _addresses;
            set
            {
                _addresses = value;
                RaisePropertyChanged(() => Addresses);
            }
        }

        public ICommand SelectAddressCommand
        {
            get
            {
                return new MvxCommand<AddressViewModel>(address =>
                {
                    Realm.GetInstance().Write(() => Realm.GetInstance().Add(address.Location, true));
                    _navigationService.Close(this);
                });
            }
        }

        public async void SearchAddress(string searchText)
        {
            if (string.IsNullOrWhiteSpace(searchText))
                return;

            Addresses.Clear();
            var result = await _webApiService.Do(async () => { return await _webApiService.Geocode(searchText); });
            if (result.Success)
            {       
                foreach (var item in result.Results)
                    Addresses.Add(new AddressViewModel { Address = item.FormattedAddress, Location = new LocationModel { Address = item.FormattedAddress, Latitude = item.Geometry.Location.Latitude, Longitude = item.Geometry.Location.Longitude } });
            }
        }

    }
}
