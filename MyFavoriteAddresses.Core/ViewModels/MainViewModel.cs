﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyFavoriteAddresses.Core.Models;
using Realms;

namespace MyFavoriteAddresses.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public delegate void AddressesLoadedHandler(List<LocationModel> locations);

        public delegate void AddressesSelectedHandler(LocationModel location);

        public event AddressesLoadedHandler AddressesLoaded;

        public event AddressesSelectedHandler AddressesSelected;

        public MainViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override Task Initialize()
        {
            Addresses = new MvxObservableCollection<AddressViewModel>();
            return base.Initialize();        
        }

        private MvxObservableCollection<AddressViewModel> _addresses;
        public MvxObservableCollection<AddressViewModel> Addresses
        {
            get => _addresses;
            set
            {
                _addresses = value;
                RaisePropertyChanged(() => Addresses);
            }
        }

        private bool _hasRecords;
        public bool HasRecords
        {
            get => _hasRecords;
            set
            {
                _hasRecords = value;
                RaisePropertyChanged();
            }
        }

        public override void ViewAppearing()
        {
            base.ViewAppearing();
            LoadAddresses();
        }

        public ICommand SelectAddressCommand => new MvxCommand<AddressViewModel>(address => { AddressesSelected?.Invoke(address.Location); });

        public ICommand NewAddressCommand => new MvxCommand(() => _navigationService.Navigate<NewAddressViewModel>());


        public void LoadAddresses()
        {
            var locations = Realm.GetInstance().All<LocationModel>().ToList();
            AddressesLoaded?.Invoke(locations);
            HasRecords = !(locations.Count > 0);
            Addresses.Clear();
            foreach (var item in locations)
                Addresses.Add(new AddressViewModel { Address = item.Address, Location = item });       
        }
    }
}
